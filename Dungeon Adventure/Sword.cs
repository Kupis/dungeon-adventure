﻿using System;
using System.Drawing;

namespace Dungeon_Adventure
{
    class Sword : Weapon
    {
        public Sword(Game game, Point location)
            : base (game, location) { }

        public override string Name { get { return "Sword"; } }

        public override void Attack(Direction direction)
        {
                if (DamageEnemy(direction, 20, 3))
                    return;

                direction += 1;
                if (DamageEnemy(direction, 20, 3))
                    return;

                if ((int)direction >= 2)
                    direction -= 2;
                else
                    direction += 2;

                DamageEnemy(direction, 10, 3);
        }
    }
}
