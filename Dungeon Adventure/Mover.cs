﻿using System;
using System.Drawing;

namespace Dungeon_Adventure
{
    abstract class Mover
    {
        private const int MoveInterval = 10;
        protected Point location;
        public Point Location { get { return location; } }
        protected Game game;

        public Mover(Game game, Point location)
        {
            this.game = game;
            this.location = location;
        }

        public bool Nearby(Point locationToCheck, int distance)
        {
            return (Nearby(location, locationToCheck, distance));
        }

        public bool Nearby(Point locationTarget, Point locationToCheck, int distance)
        {
            if (Math.Abs(locationTarget.X - locationToCheck.X) <= distance &&
                Math.Abs(locationTarget.Y - locationToCheck.Y) <= distance)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected Direction FindDDirection(Point enemyLocation, Point playerLocation)
        {
            Direction direction;

            if (enemyLocation.X > playerLocation.X + 10)
                direction = Direction.Right;
            else if (enemyLocation.X < playerLocation.X - 10)
                direction = Direction.Left;
            else if (enemyLocation.Y < playerLocation.Y - 10)
                direction = Direction.Up;
            else
                direction = Direction.Down;

            return direction;
        }

        public Point Move(Direction direction, Rectangle boundaries)
        {
            return Move(direction, location, boundaries);
        }

        public Point Move(Direction direction, Point target, Rectangle boundaries)
        {
            Point newLocation = target;

            switch (direction)
            {
                case Direction.Up:
                    if (newLocation.Y - MoveInterval >= boundaries.Top)
                        newLocation.Y -= MoveInterval;
                    break;
                case Direction.Down:
                    if (newLocation.Y + MoveInterval <= boundaries.Bottom)
                        newLocation.Y += MoveInterval;
                    break;
                case Direction.Left:
                    if (newLocation.X - MoveInterval >= boundaries.Left)
                        newLocation.X -= MoveInterval;
                    break;
                case Direction.Right:
                    if (newLocation.X + MoveInterval <= boundaries.Right)
                        newLocation.X += MoveInterval;
                    break;
                default: break;
            }
            return newLocation;
        }
    }
}
