﻿using System;
using System.Drawing;

namespace Dungeon_Adventure
{
    class Mace : Weapon
    {
        public Mace(Game game, Point location)
            : base (game, location) { }

        public override string Name { get { return "Mace"; } }

        public override void Attack(Direction direction)
        {
            foreach (Direction directionLoop in Enum.GetValues(typeof(Direction)))
            DamageEnemy(directionLoop, 30, 6);
        }
    }
}
