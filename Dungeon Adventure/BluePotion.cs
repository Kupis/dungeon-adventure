﻿using System;
using System.Drawing;

namespace Dungeon_Adventure
{
    class BluePotion : Weapon, IPotion
    {
        public BluePotion(Game game, Point location)
            : base (game, location)
        {
            Used = false;
        }

        public override string Name { get { return "Blue potion"; } }

        public bool Used { get; private set; }

        public override void Attack(Direction direction)
        {
            if (!Used)
            {
                Used = true;
                game.IncreasePlayerHealth(5);
            }
        }
    }
}
