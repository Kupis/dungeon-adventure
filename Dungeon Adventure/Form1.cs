﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Dungeon_Adventure
{
    public partial class mainForm : Form
    {
        private Game game;
        int enemiesShown;

        public mainForm()
        {
            InitializeComponent();
        }

        private void mainForm_Load(object sender, EventArgs e)
        {
            game = new Game(new Rectangle(76, 57, 430, 160));
            game.NewLevel();
            giveEnemiesFormControls();
            UpdateForm();
        }

        private void giveEnemiesFormControls()
        {
            foreach (Enemy enemy in game.Enemies)
            {
                if (enemy is Bat)
                {
                    enemy.pictureBox = batPictureBox;
                    enemy.healthLabel = batHPLabel;
                    enemy.nameLabel = batLabel;
                }
                else if (enemy is Ghost)
                {
                    enemy.pictureBox = ghostPictureBox;
                    enemy.healthLabel = ghostHPLabel;
                    enemy.nameLabel = ghostLabel;
                }
                else if (enemy is Ghoul)
                {
                    enemy.pictureBox = ghoulPictureBox;
                    enemy.healthLabel = ghoulHPLabel;
                    enemy.nameLabel = ghoulLabel;
                }
            }
        }

        public void UpdateForm()
        {
            updateInfo();
            updatePlayer();
            updateEnemy();
            updateWeapons();
            updateEQ();
            checkIfPlayerIsDead();
            checkIfEnemiesAreDead();
        }

        private void updateInfo()
        {
            coordinateLabel.Text = "Player: " + game.PlayerLocation.ToString() + Environment.NewLine;
            levelLabel.Text = "Level: " + game.Level.ToString();
            logLabel.Text = Game.Logs.ToString(); ;
        }

        private void updatePlayer()
        {
            playerPictureBox.Location = game.PlayerLocation;
            playerHPLabel.Text = game.PlayerHitPoints.ToString();
        }

        private void updateEnemy()
        {
            enemiesShown = 0;

            foreach (Enemy enemy in game.Enemies)
            {
                coordinateLabel.Text += enemy.Name + ": " + enemy.Location.ToString() + Environment.NewLine;
                enemy.pictureBox.Location = enemy.Location;
                enemy.healthLabel.Text = enemy.HitPoints.ToString();
                if (enemy.HitPoints > 0)
                {
                    enemy.showControls();
                    enemiesShown++;
                }
                else
                    enemy.hideControls();
            }
        }

        private void updateWeapons()
        {
            swordPictureBox.Visible = false;
            bowPictureBox.Visible = false;
            macePictureBox.Visible = false;
            bluePotionPictureBox.Visible = false;
            redPotionPictureBox.Visible = false;

            Control weaponControl = null;
            switch (game.WeaponInRoom.Name)
            {
                case "Sword":
                    weaponControl = swordPictureBox;
                    break;
                case "Bow":
                    weaponControl = bowPictureBox;
                    break;
                case "Mace":
                    weaponControl = macePictureBox;
                    break;
                case "Blue potion":
                    weaponControl = bluePotionPictureBox;
                    break;
                case "Red potion":
                    weaponControl = redPotionPictureBox;
                    break;
            }

            weaponControl.Location = game.WeaponInRoom.Location;
            if (game.WeaponInRoom.PickedUp)
                weaponControl.Visible = false;
            else
                weaponControl.Visible = true;
        }

        private void updateEQ()
        {
            bluePotionEQPictureBox.Visible = false;
            redPotionEQPictureBox.Visible = false;

            if (game.CheckPlayerInventory("Sword"))
                swordEQPictureBox.Visible = true;
            if (game.CheckPlayerInventory("Bow"))
                bowEQPictureBox.Visible = true;
            if (game.CheckPlayerInventory("Mace"))
                maceEQPictureBox.Visible = true;
            if (game.CheckPlayerInventory("Blue potion"))
                bluePotionEQPictureBox.Visible = true;
            if (game.CheckPlayerInventory("Red potion"))
                redPotionEQPictureBox.Visible = true;
        }

        private void checkIfPlayerIsDead()
        {
            if (game.PlayerHitPoints <= 0)
            {
                moveUpButton.Visible = false;
                moveLeftButton.Visible = false;
                moveDownButton.Visible = false;
                moveUpButton.Visible = false;

                attackUpButton.Visible = false;
                attackLeftButton.Visible = false;
                attackDownButton.Visible = false;
                attackRightButton.Visible = false;

                playerPictureBox.Visible = false;

                MessageBox.Show("YOU DIED! PRESS OK TO RESTART GAME", "GAME OVER", MessageBoxButtons.OK);

                Application.Restart();
            }
        }

        private void checkIfEnemiesAreDead()
        {
            if (enemiesShown < 1)
            {
                MessageBox.Show("All enemies were killed, do you want to move to the next level?", "Move to the next level", MessageBoxButtons.OK);
                game.NewLevel();
                giveEnemiesFormControls();
                UpdateForm();
            }
        }

        private void moveUpButton_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Up);
            UpdateForm();
        }

        private void moveLeftButton_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Left);
            UpdateForm();
        }

        private void moveDownButton_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Down);
            UpdateForm();
        }

        private void moveRightButton_Click(object sender, EventArgs e)
        {
            game.Move(Direction.Right);
            UpdateForm();
        }

        private void attackUpButton_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Up);
            UpdateForm();
        }

        private void attackLeftButton_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Left);
            UpdateForm();
        }

        private void attackDownButton_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Down);
            UpdateForm();
        }

        private void attackRightButton_Click(object sender, EventArgs e)
        {
            game.Attack(Direction.Right);
            UpdateForm();
        }

        private void equipWeapon(PictureBox control)
        {
            swordEQPictureBox.BorderStyle = BorderStyle.None;
            bowEQPictureBox.BorderStyle = BorderStyle.None;
            maceEQPictureBox.BorderStyle = BorderStyle.None;
            bluePotionEQPictureBox.BorderStyle = BorderStyle.None;
            redPotionEQPictureBox.BorderStyle = BorderStyle.None;

            control.BorderStyle = BorderStyle.FixedSingle;
        }

        private void swordEQPictureBox_Click(object sender, EventArgs e)
        {
            game.Equip("Sword");
            equipWeapon(swordEQPictureBox);
        }

        private void bowEQPictureBx_Click(object sender, EventArgs e)
        {
            game.Equip("Bow");
            equipWeapon(bowEQPictureBox);
        }

        private void maceEQPictureBox_Click(object sender, EventArgs e)
        {
            game.Equip("Mace");
            equipWeapon(maceEQPictureBox);
        }

        private void bluePotionEQPictureBox_Click(object sender, EventArgs e)
        {
            game.Equip("Blue potion");
            equipWeapon(bluePotionEQPictureBox);
        }

        private void redPotionEQPictureBox_Click(object sender, EventArgs e)
        {
            game.Equip("Red potion");
            equipWeapon(redPotionEQPictureBox);
        }

        private void mainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.W)
                moveUpButton_Click(sender, e);
            else if (e.KeyCode == Keys.A)
                moveLeftButton_Click(sender, e);
            else if (e.KeyCode == Keys.S)
                moveDownButton_Click(sender, e);
            else if (e.KeyCode == Keys.D)
                moveRightButton_Click(sender, e);

            if (e.KeyCode == Keys.I)
                attackUpButton_Click(sender, e);
            else if (e.KeyCode == Keys.J)
                attackLeftButton_Click(sender, e);
            else if (e.KeyCode == Keys.K)
                attackDownButton_Click(sender, e);
            else if (e.KeyCode == Keys.L)
                attackRightButton_Click(sender, e);
        }
    }
}
