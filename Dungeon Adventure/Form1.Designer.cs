﻿namespace Dungeon_Adventure
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.swordEQPictureBox = new System.Windows.Forms.PictureBox();
            this.bowEQPictureBox = new System.Windows.Forms.PictureBox();
            this.maceEQPictureBox = new System.Windows.Forms.PictureBox();
            this.bluePotionEQPictureBox = new System.Windows.Forms.PictureBox();
            this.redPotionEQPictureBox = new System.Windows.Forms.PictureBox();
            this.moveGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.moveRightButton = new System.Windows.Forms.Button();
            this.moveDownButton = new System.Windows.Forms.Button();
            this.moveLeftButton = new System.Windows.Forms.Button();
            this.moveUpButton = new System.Windows.Forms.Button();
            this.attackGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.attackRightButton = new System.Windows.Forms.Button();
            this.attackDownButton = new System.Windows.Forms.Button();
            this.attackLeftButton = new System.Windows.Forms.Button();
            this.attackUpButton = new System.Windows.Forms.Button();
            this.playerPictureBox = new System.Windows.Forms.PictureBox();
            this.batPictureBox = new System.Windows.Forms.PictureBox();
            this.ghostPictureBox = new System.Windows.Forms.PictureBox();
            this.ghoulPictureBox = new System.Windows.Forms.PictureBox();
            this.bluePotionPictureBox = new System.Windows.Forms.PictureBox();
            this.redPotionPictureBox = new System.Windows.Forms.PictureBox();
            this.swordPictureBox = new System.Windows.Forms.PictureBox();
            this.bowPictureBox = new System.Windows.Forms.PictureBox();
            this.macePictureBox = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.playerLabel = new System.Windows.Forms.Label();
            this.playerHPLabel = new System.Windows.Forms.Label();
            this.batLabel = new System.Windows.Forms.Label();
            this.batHPLabel = new System.Windows.Forms.Label();
            this.ghostLabel = new System.Windows.Forms.Label();
            this.ghostHPLabel = new System.Windows.Forms.Label();
            this.ghoulLabel = new System.Windows.Forms.Label();
            this.ghoulHPLabel = new System.Windows.Forms.Label();
            this.logLabel = new System.Windows.Forms.Label();
            this.coordinateLabel = new System.Windows.Forms.Label();
            this.levelLabel = new System.Windows.Forms.Label();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.swordEQPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bowEQPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maceEQPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bluePotionEQPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redPotionEQPictureBox)).BeginInit();
            this.moveGroupBox.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.attackGroupBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.batPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghostPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghoulPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bluePotionPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redPotionPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.swordPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bowPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.macePictureBox)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // swordEQPictureBox
            // 
            this.swordEQPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.swordEQPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.sword;
            this.swordEQPictureBox.Location = new System.Drawing.Point(68, 320);
            this.swordEQPictureBox.Name = "swordEQPictureBox";
            this.swordEQPictureBox.Size = new System.Drawing.Size(50, 50);
            this.swordEQPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.swordEQPictureBox.TabIndex = 0;
            this.swordEQPictureBox.TabStop = false;
            this.swordEQPictureBox.Visible = false;
            this.swordEQPictureBox.Click += new System.EventHandler(this.swordEQPictureBox_Click);
            // 
            // bowEQPictureBox
            // 
            this.bowEQPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.bowEQPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.bow;
            this.bowEQPictureBox.Location = new System.Drawing.Point(124, 320);
            this.bowEQPictureBox.Name = "bowEQPictureBox";
            this.bowEQPictureBox.Size = new System.Drawing.Size(50, 50);
            this.bowEQPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bowEQPictureBox.TabIndex = 1;
            this.bowEQPictureBox.TabStop = false;
            this.bowEQPictureBox.Visible = false;
            this.bowEQPictureBox.Click += new System.EventHandler(this.bowEQPictureBx_Click);
            // 
            // maceEQPictureBox
            // 
            this.maceEQPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.maceEQPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.mace;
            this.maceEQPictureBox.Location = new System.Drawing.Point(180, 320);
            this.maceEQPictureBox.Name = "maceEQPictureBox";
            this.maceEQPictureBox.Size = new System.Drawing.Size(50, 50);
            this.maceEQPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.maceEQPictureBox.TabIndex = 2;
            this.maceEQPictureBox.TabStop = false;
            this.maceEQPictureBox.Visible = false;
            this.maceEQPictureBox.Click += new System.EventHandler(this.maceEQPictureBox_Click);
            // 
            // bluePotionEQPictureBox
            // 
            this.bluePotionEQPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.bluePotionEQPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.potion_blue;
            this.bluePotionEQPictureBox.Location = new System.Drawing.Point(236, 320);
            this.bluePotionEQPictureBox.Name = "bluePotionEQPictureBox";
            this.bluePotionEQPictureBox.Size = new System.Drawing.Size(50, 50);
            this.bluePotionEQPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.bluePotionEQPictureBox.TabIndex = 3;
            this.bluePotionEQPictureBox.TabStop = false;
            this.bluePotionEQPictureBox.Visible = false;
            this.bluePotionEQPictureBox.Click += new System.EventHandler(this.bluePotionEQPictureBox_Click);
            // 
            // redPotionEQPictureBox
            // 
            this.redPotionEQPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.redPotionEQPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.potion_red;
            this.redPotionEQPictureBox.Location = new System.Drawing.Point(292, 320);
            this.redPotionEQPictureBox.Name = "redPotionEQPictureBox";
            this.redPotionEQPictureBox.Size = new System.Drawing.Size(50, 50);
            this.redPotionEQPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.redPotionEQPictureBox.TabIndex = 4;
            this.redPotionEQPictureBox.TabStop = false;
            this.redPotionEQPictureBox.Visible = false;
            this.redPotionEQPictureBox.Click += new System.EventHandler(this.redPotionEQPictureBox_Click);
            // 
            // moveGroupBox
            // 
            this.moveGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.moveGroupBox.Controls.Add(this.tableLayoutPanel1);
            this.moveGroupBox.Location = new System.Drawing.Point(348, 309);
            this.moveGroupBox.Name = "moveGroupBox";
            this.moveGroupBox.Size = new System.Drawing.Size(95, 72);
            this.moveGroupBox.TabIndex = 0;
            this.moveGroupBox.TabStop = false;
            this.moveGroupBox.Text = "Move";
            this.ToolTip.SetToolTip(this.moveGroupBox, "Move using:    W\r\n                        ASD");
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.moveRightButton, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.moveDownButton, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.moveLeftButton, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.moveUpButton, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(89, 53);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // moveRightButton
            // 
            this.moveRightButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moveRightButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.moveRightButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.moveRightButton.Location = new System.Drawing.Point(59, 27);
            this.moveRightButton.Margin = new System.Windows.Forms.Padding(1);
            this.moveRightButton.Name = "moveRightButton";
            this.moveRightButton.Size = new System.Drawing.Size(29, 25);
            this.moveRightButton.TabIndex = 3;
            this.moveRightButton.Text = "→";
            this.ToolTip.SetToolTip(this.moveRightButton, "Move using:    D");
            this.moveRightButton.UseVisualStyleBackColor = true;
            this.moveRightButton.Click += new System.EventHandler(this.moveRightButton_Click);
            // 
            // moveDownButton
            // 
            this.moveDownButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moveDownButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.moveDownButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.moveDownButton.Location = new System.Drawing.Point(30, 27);
            this.moveDownButton.Margin = new System.Windows.Forms.Padding(1);
            this.moveDownButton.Name = "moveDownButton";
            this.moveDownButton.Size = new System.Drawing.Size(27, 25);
            this.moveDownButton.TabIndex = 2;
            this.moveDownButton.Text = "↓";
            this.ToolTip.SetToolTip(this.moveDownButton, "Move using:    S");
            this.moveDownButton.UseVisualStyleBackColor = true;
            this.moveDownButton.Click += new System.EventHandler(this.moveDownButton_Click);
            // 
            // moveLeftButton
            // 
            this.moveLeftButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moveLeftButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.moveLeftButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.moveLeftButton.Location = new System.Drawing.Point(1, 27);
            this.moveLeftButton.Margin = new System.Windows.Forms.Padding(1);
            this.moveLeftButton.Name = "moveLeftButton";
            this.moveLeftButton.Size = new System.Drawing.Size(27, 25);
            this.moveLeftButton.TabIndex = 1;
            this.moveLeftButton.Text = "←";
            this.ToolTip.SetToolTip(this.moveLeftButton, "Move using:    A");
            this.moveLeftButton.UseVisualStyleBackColor = true;
            this.moveLeftButton.Click += new System.EventHandler(this.moveLeftButton_Click);
            // 
            // moveUpButton
            // 
            this.moveUpButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moveUpButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.moveUpButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.moveUpButton.Location = new System.Drawing.Point(30, 1);
            this.moveUpButton.Margin = new System.Windows.Forms.Padding(1);
            this.moveUpButton.Name = "moveUpButton";
            this.moveUpButton.Size = new System.Drawing.Size(27, 24);
            this.moveUpButton.TabIndex = 0;
            this.moveUpButton.Text = "↑";
            this.ToolTip.SetToolTip(this.moveUpButton, "Move using:    W");
            this.moveUpButton.UseVisualStyleBackColor = true;
            this.moveUpButton.Click += new System.EventHandler(this.moveUpButton_Click);
            // 
            // attackGroupBox
            // 
            this.attackGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.attackGroupBox.Controls.Add(this.tableLayoutPanel2);
            this.attackGroupBox.Location = new System.Drawing.Point(449, 309);
            this.attackGroupBox.Name = "attackGroupBox";
            this.attackGroupBox.Size = new System.Drawing.Size(95, 72);
            this.attackGroupBox.TabIndex = 1;
            this.attackGroupBox.TabStop = false;
            this.attackGroupBox.Text = "Attack";
            this.ToolTip.SetToolTip(this.attackGroupBox, "Attack using:    I\r\n                         JKL\r\n");
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.attackRightButton, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.attackDownButton, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.attackLeftButton, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.attackUpButton, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(89, 53);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // attackRightButton
            // 
            this.attackRightButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attackRightButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.attackRightButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.attackRightButton.Location = new System.Drawing.Point(59, 27);
            this.attackRightButton.Margin = new System.Windows.Forms.Padding(1);
            this.attackRightButton.Name = "attackRightButton";
            this.attackRightButton.Size = new System.Drawing.Size(29, 25);
            this.attackRightButton.TabIndex = 3;
            this.attackRightButton.Text = "→";
            this.ToolTip.SetToolTip(this.attackRightButton, "Attack using:    L");
            this.attackRightButton.UseVisualStyleBackColor = true;
            this.attackRightButton.Click += new System.EventHandler(this.attackRightButton_Click);
            // 
            // attackDownButton
            // 
            this.attackDownButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attackDownButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.attackDownButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.attackDownButton.Location = new System.Drawing.Point(30, 27);
            this.attackDownButton.Margin = new System.Windows.Forms.Padding(1);
            this.attackDownButton.Name = "attackDownButton";
            this.attackDownButton.Size = new System.Drawing.Size(27, 25);
            this.attackDownButton.TabIndex = 2;
            this.attackDownButton.Text = "↓";
            this.ToolTip.SetToolTip(this.attackDownButton, "Attack using:    K");
            this.attackDownButton.UseVisualStyleBackColor = true;
            this.attackDownButton.Click += new System.EventHandler(this.attackDownButton_Click);
            // 
            // attackLeftButton
            // 
            this.attackLeftButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attackLeftButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.attackLeftButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.attackLeftButton.Location = new System.Drawing.Point(1, 27);
            this.attackLeftButton.Margin = new System.Windows.Forms.Padding(1);
            this.attackLeftButton.Name = "attackLeftButton";
            this.attackLeftButton.Size = new System.Drawing.Size(27, 25);
            this.attackLeftButton.TabIndex = 1;
            this.attackLeftButton.Text = "←";
            this.ToolTip.SetToolTip(this.attackLeftButton, "Attack using:    J");
            this.attackLeftButton.UseVisualStyleBackColor = true;
            this.attackLeftButton.Click += new System.EventHandler(this.attackLeftButton_Click);
            // 
            // attackUpButton
            // 
            this.attackUpButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attackUpButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.attackUpButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.attackUpButton.Location = new System.Drawing.Point(30, 1);
            this.attackUpButton.Margin = new System.Windows.Forms.Padding(1);
            this.attackUpButton.Name = "attackUpButton";
            this.attackUpButton.Size = new System.Drawing.Size(27, 24);
            this.attackUpButton.TabIndex = 0;
            this.attackUpButton.Text = "↑";
            this.ToolTip.SetToolTip(this.attackUpButton, "Attack using:    I");
            this.attackUpButton.UseVisualStyleBackColor = true;
            this.attackUpButton.Click += new System.EventHandler(this.attackUpButton_Click);
            // 
            // playerPictureBox
            // 
            this.playerPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.playerPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.player;
            this.playerPictureBox.Location = new System.Drawing.Point(88, 63);
            this.playerPictureBox.Name = "playerPictureBox";
            this.playerPictureBox.Size = new System.Drawing.Size(30, 30);
            this.playerPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playerPictureBox.TabIndex = 6;
            this.playerPictureBox.TabStop = false;
            // 
            // batPictureBox
            // 
            this.batPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.batPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.bat;
            this.batPictureBox.Location = new System.Drawing.Point(124, 63);
            this.batPictureBox.Name = "batPictureBox";
            this.batPictureBox.Size = new System.Drawing.Size(30, 30);
            this.batPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.batPictureBox.TabIndex = 7;
            this.batPictureBox.TabStop = false;
            this.batPictureBox.Visible = false;
            // 
            // ghostPictureBox
            // 
            this.ghostPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.ghostPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.ghost;
            this.ghostPictureBox.Location = new System.Drawing.Point(160, 63);
            this.ghostPictureBox.Name = "ghostPictureBox";
            this.ghostPictureBox.Size = new System.Drawing.Size(30, 30);
            this.ghostPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ghostPictureBox.TabIndex = 8;
            this.ghostPictureBox.TabStop = false;
            this.ghostPictureBox.Visible = false;
            // 
            // ghoulPictureBox
            // 
            this.ghoulPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.ghoulPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.ghoul;
            this.ghoulPictureBox.Location = new System.Drawing.Point(196, 63);
            this.ghoulPictureBox.Name = "ghoulPictureBox";
            this.ghoulPictureBox.Size = new System.Drawing.Size(30, 30);
            this.ghoulPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ghoulPictureBox.TabIndex = 9;
            this.ghoulPictureBox.TabStop = false;
            this.ghoulPictureBox.Visible = false;
            // 
            // bluePotionPictureBox
            // 
            this.bluePotionPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.bluePotionPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.potion_blue;
            this.bluePotionPictureBox.Location = new System.Drawing.Point(232, 63);
            this.bluePotionPictureBox.Name = "bluePotionPictureBox";
            this.bluePotionPictureBox.Size = new System.Drawing.Size(30, 30);
            this.bluePotionPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bluePotionPictureBox.TabIndex = 10;
            this.bluePotionPictureBox.TabStop = false;
            this.bluePotionPictureBox.Visible = false;
            // 
            // redPotionPictureBox
            // 
            this.redPotionPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.redPotionPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.potion_red;
            this.redPotionPictureBox.Location = new System.Drawing.Point(268, 63);
            this.redPotionPictureBox.Name = "redPotionPictureBox";
            this.redPotionPictureBox.Size = new System.Drawing.Size(30, 30);
            this.redPotionPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.redPotionPictureBox.TabIndex = 11;
            this.redPotionPictureBox.TabStop = false;
            this.redPotionPictureBox.Visible = false;
            // 
            // swordPictureBox
            // 
            this.swordPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.swordPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.sword;
            this.swordPictureBox.Location = new System.Drawing.Point(304, 63);
            this.swordPictureBox.Name = "swordPictureBox";
            this.swordPictureBox.Size = new System.Drawing.Size(30, 30);
            this.swordPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.swordPictureBox.TabIndex = 12;
            this.swordPictureBox.TabStop = false;
            this.swordPictureBox.Visible = false;
            // 
            // bowPictureBox
            // 
            this.bowPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.bowPictureBox.Image = global::Dungeon_Adventure.Properties.Resources.bow;
            this.bowPictureBox.Location = new System.Drawing.Point(340, 63);
            this.bowPictureBox.Name = "bowPictureBox";
            this.bowPictureBox.Size = new System.Drawing.Size(30, 30);
            this.bowPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bowPictureBox.TabIndex = 13;
            this.bowPictureBox.TabStop = false;
            this.bowPictureBox.Visible = false;
            // 
            // macePictureBox
            // 
            this.macePictureBox.BackColor = System.Drawing.Color.Transparent;
            this.macePictureBox.Image = global::Dungeon_Adventure.Properties.Resources.mace;
            this.macePictureBox.Location = new System.Drawing.Point(376, 63);
            this.macePictureBox.Name = "macePictureBox";
            this.macePictureBox.Size = new System.Drawing.Size(30, 30);
            this.macePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.macePictureBox.TabIndex = 14;
            this.macePictureBox.TabStop = false;
            this.macePictureBox.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.Controls.Add(this.playerLabel, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.playerHPLabel, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.batLabel, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.batHPLabel, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.ghostLabel, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.ghostHPLabel, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.ghoulLabel, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.ghoulHPLabel, 1, 3);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(453, 246);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(77, 57);
            this.tableLayoutPanel3.TabIndex = 15;
            // 
            // playerLabel
            // 
            this.playerLabel.AutoSize = true;
            this.playerLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playerLabel.Location = new System.Drawing.Point(3, 0);
            this.playerLabel.Name = "playerLabel";
            this.playerLabel.Size = new System.Drawing.Size(36, 14);
            this.playerLabel.TabIndex = 0;
            this.playerLabel.Text = "Player";
            // 
            // playerHPLabel
            // 
            this.playerHPLabel.AutoSize = true;
            this.playerHPLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playerHPLabel.Location = new System.Drawing.Point(45, 0);
            this.playerHPLabel.Name = "playerHPLabel";
            this.playerHPLabel.Size = new System.Drawing.Size(29, 14);
            this.playerHPLabel.TabIndex = 1;
            this.playerHPLabel.Text = "10";
            // 
            // batLabel
            // 
            this.batLabel.AutoSize = true;
            this.batLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.batLabel.Location = new System.Drawing.Point(3, 14);
            this.batLabel.Name = "batLabel";
            this.batLabel.Size = new System.Drawing.Size(36, 14);
            this.batLabel.TabIndex = 2;
            this.batLabel.Text = "Bat";
            this.batLabel.Visible = false;
            // 
            // batHPLabel
            // 
            this.batHPLabel.AutoSize = true;
            this.batHPLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.batHPLabel.Location = new System.Drawing.Point(45, 14);
            this.batHPLabel.Name = "batHPLabel";
            this.batHPLabel.Size = new System.Drawing.Size(29, 14);
            this.batHPLabel.TabIndex = 3;
            this.batHPLabel.Text = "6";
            this.batHPLabel.Visible = false;
            // 
            // ghostLabel
            // 
            this.ghostLabel.AutoSize = true;
            this.ghostLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ghostLabel.Location = new System.Drawing.Point(3, 28);
            this.ghostLabel.Name = "ghostLabel";
            this.ghostLabel.Size = new System.Drawing.Size(36, 14);
            this.ghostLabel.TabIndex = 4;
            this.ghostLabel.Text = "Ghost";
            this.ghostLabel.Visible = false;
            // 
            // ghostHPLabel
            // 
            this.ghostHPLabel.AutoSize = true;
            this.ghostHPLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ghostHPLabel.Location = new System.Drawing.Point(45, 28);
            this.ghostHPLabel.Name = "ghostHPLabel";
            this.ghostHPLabel.Size = new System.Drawing.Size(29, 14);
            this.ghostHPLabel.TabIndex = 5;
            this.ghostHPLabel.Text = "8";
            this.ghostHPLabel.Visible = false;
            // 
            // ghoulLabel
            // 
            this.ghoulLabel.AutoSize = true;
            this.ghoulLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ghoulLabel.Location = new System.Drawing.Point(3, 42);
            this.ghoulLabel.Name = "ghoulLabel";
            this.ghoulLabel.Size = new System.Drawing.Size(36, 15);
            this.ghoulLabel.TabIndex = 6;
            this.ghoulLabel.Text = "Ghoul";
            this.ghoulLabel.Visible = false;
            // 
            // ghoulHPLabel
            // 
            this.ghoulHPLabel.AutoSize = true;
            this.ghoulHPLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ghoulHPLabel.Location = new System.Drawing.Point(45, 42);
            this.ghoulHPLabel.Name = "ghoulHPLabel";
            this.ghoulHPLabel.Size = new System.Drawing.Size(29, 15);
            this.ghoulHPLabel.TabIndex = 7;
            this.ghoulHPLabel.Text = "10";
            this.ghoulHPLabel.Visible = false;
            // 
            // logLabel
            // 
            this.logLabel.BackColor = System.Drawing.Color.Transparent;
            this.logLabel.Location = new System.Drawing.Point(268, 246);
            this.logLabel.Name = "logLabel";
            this.logLabel.Size = new System.Drawing.Size(179, 57);
            this.logLabel.TabIndex = 16;
            this.logLabel.Text = "Logs";
            // 
            // coordinateLabel
            // 
            this.coordinateLabel.BackColor = System.Drawing.Color.Transparent;
            this.coordinateLabel.Location = new System.Drawing.Point(242, 0);
            this.coordinateLabel.Name = "coordinateLabel";
            this.coordinateLabel.Size = new System.Drawing.Size(118, 54);
            this.coordinateLabel.TabIndex = 17;
            this.coordinateLabel.Text = "Coordinate";
            // 
            // levelLabel
            // 
            this.levelLabel.AutoSize = true;
            this.levelLabel.BackColor = System.Drawing.Color.Transparent;
            this.levelLabel.Location = new System.Drawing.Point(479, 0);
            this.levelLabel.Name = "levelLabel";
            this.levelLabel.Size = new System.Drawing.Size(45, 13);
            this.levelLabel.TabIndex = 18;
            this.levelLabel.Text = "Level: 0";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Dungeon_Adventure.Properties.Resources.dungeon600x400;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(604, 402);
            this.Controls.Add(this.levelLabel);
            this.Controls.Add(this.coordinateLabel);
            this.Controls.Add(this.playerPictureBox);
            this.Controls.Add(this.logLabel);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.attackGroupBox);
            this.Controls.Add(this.moveGroupBox);
            this.Controls.Add(this.redPotionEQPictureBox);
            this.Controls.Add(this.bluePotionEQPictureBox);
            this.Controls.Add(this.maceEQPictureBox);
            this.Controls.Add(this.bowEQPictureBox);
            this.Controls.Add(this.swordEQPictureBox);
            this.Controls.Add(this.batPictureBox);
            this.Controls.Add(this.ghostPictureBox);
            this.Controls.Add(this.ghoulPictureBox);
            this.Controls.Add(this.bluePotionPictureBox);
            this.Controls.Add(this.redPotionPictureBox);
            this.Controls.Add(this.swordPictureBox);
            this.Controls.Add(this.bowPictureBox);
            this.Controls.Add(this.macePictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.Text = "Dungeon";
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mainForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.swordEQPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bowEQPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maceEQPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bluePotionEQPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redPotionEQPictureBox)).EndInit();
            this.moveGroupBox.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.attackGroupBox.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playerPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.batPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghostPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ghoulPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bluePotionPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redPotionPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.swordPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bowPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.macePictureBox)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox swordEQPictureBox;
        private System.Windows.Forms.PictureBox bowEQPictureBox;
        private System.Windows.Forms.PictureBox maceEQPictureBox;
        private System.Windows.Forms.PictureBox bluePotionEQPictureBox;
        private System.Windows.Forms.PictureBox redPotionEQPictureBox;
        private System.Windows.Forms.GroupBox moveGroupBox;
        private System.Windows.Forms.GroupBox attackGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button moveRightButton;
        private System.Windows.Forms.Button moveDownButton;
        private System.Windows.Forms.Button moveLeftButton;
        private System.Windows.Forms.Button moveUpButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button attackRightButton;
        private System.Windows.Forms.Button attackDownButton;
        private System.Windows.Forms.Button attackLeftButton;
        private System.Windows.Forms.Button attackUpButton;
        private System.Windows.Forms.PictureBox playerPictureBox;
        private System.Windows.Forms.PictureBox batPictureBox;
        private System.Windows.Forms.PictureBox ghostPictureBox;
        private System.Windows.Forms.PictureBox ghoulPictureBox;
        private System.Windows.Forms.PictureBox bluePotionPictureBox;
        private System.Windows.Forms.PictureBox redPotionPictureBox;
        private System.Windows.Forms.PictureBox swordPictureBox;
        private System.Windows.Forms.PictureBox bowPictureBox;
        private System.Windows.Forms.PictureBox macePictureBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label playerLabel;
        private System.Windows.Forms.Label playerHPLabel;
        private System.Windows.Forms.Label batHPLabel;
        private System.Windows.Forms.Label ghostLabel;
        private System.Windows.Forms.Label ghostHPLabel;
        private System.Windows.Forms.Label ghoulLabel;
        private System.Windows.Forms.Label ghoulHPLabel;
        private System.Windows.Forms.Label logLabel;
        private System.Windows.Forms.Label coordinateLabel;
        private System.Windows.Forms.Label levelLabel;
        private System.Windows.Forms.ToolTip ToolTip;
        public System.Windows.Forms.Label batLabel;
    }
}

