﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Dungeon_Adventure
{
    abstract class Enemy : Mover
    {
        private const int NearPlayerDistance = 10;

        public string Name;
        public int HitPoints { get; private set; }

        public PictureBox pictureBox;
        public Label healthLabel;
        public Label nameLabel;
        
        public bool Dead
        {
            get
            {
                if (HitPoints <= 0)
                    return true;
                return false;
            }
        }

        public Enemy(Game game, Point location, int hitPoints, string name)
            : base(game, location)
        {
            this.HitPoints = hitPoints;
            this.Name = name;
        }

        public void showControls()
        {
            pictureBox.Visible = true;
            healthLabel.Visible = true;
            nameLabel.Visible = true;
        }

        public void hideControls()
        {
            pictureBox.Visible = false;
            healthLabel.Visible = false;
            nameLabel.Visible = false;
        }

        public abstract void Move();

        public void Hit(int maxDamage)
        {
            int randomDamage = Game.random.Next(1, maxDamage);
            HitPoints -= randomDamage;
            Game.Logs.AppendLine(this.Name + " took " + randomDamage.ToString() + " damage");
        }

        protected bool NearPlayer()
        {
            return (Nearby(game.PlayerLocation, NearPlayerDistance));
        }

        protected Direction FindPlayerDirection(Point playerLocation)
        {
            Direction directionToMove;

            if (playerLocation.X > location.X + 10)
                directionToMove = Direction.Right;
            else if (playerLocation.X < location.X - 10)
                directionToMove = Direction.Left;
            else if (playerLocation.Y < location.Y - 10)
                directionToMove = Direction.Up;
            else
                directionToMove = Direction.Down;

            return directionToMove;
        }
    }
}
