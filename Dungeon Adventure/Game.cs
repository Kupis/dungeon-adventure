﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using System.Text;

namespace Dungeon_Adventure
{
    class Game
    {
        public List<Enemy> Enemies { get; private set; }
        public Weapon WeaponInRoom { get; private set; }

        private Player player;
        public Point PlayerLocation { get { return player.Location; } }
        public int PlayerHitPoints { get { return player.HitPoints; } }
        public IEnumerable<string> PlayerWeapons { get { return player.Weapons; } }
        private int level = 0;
        public int Level { get { return level; } }
        public static StringBuilder Logs = new StringBuilder();
        public static Random random = new Random();

        private Rectangle boundaries;
        public Rectangle Boundaries { get { return boundaries; } }

        public Game(Rectangle boundaries)
        {
            this.boundaries = boundaries;
            player = new Player(this, new Point(boundaries.Left + 10, boundaries.Top + 70));
        }

        public void Move(Direction direction)
        {
            player.Move(direction);
            foreach (Enemy enemy in Enemies)
                enemy.Move(); //random to check TODO
        }

        public void Equip(string weaponName)
        {
            player.Equip(weaponName);
        }

        public bool CheckPlayerInventory(string weaponName)
        {
            return player.Weapons.Contains(weaponName);
        }

        public void HitPlayer(int maxDamage)
        {
            player.Hit(maxDamage);
        }

        public void IncreasePlayerHealth(int health)
        {
            player.IncreaseHealth(health);
        }

        public void Attack(Direction direction)
        {
            player.Attack(direction);
            foreach (Enemy enemy in Enemies)
                enemy.Move();
        }
        
        private Point GetRandomLocation()
        {
            return new Point(boundaries.Left + 
                random.Next(boundaries.Right / 10 - boundaries.Left / 10) * 10,
                boundaries.Top + 
                random.Next(boundaries.Bottom / 10 - boundaries.Top / 10) * 10);
        }

        public void NewLevel()
        {
            level++;
            switch(level)
            {
                case 1:
                    Enemies = new List<Enemy>() { new Bat(this, GetRandomLocation()) };
                    WeaponInRoom = new Sword(this, GetRandomLocation());
                    break;
                case 2:
                    Enemies = new List<Enemy>() { new Ghost(this, GetRandomLocation()) };
                    WeaponInRoom = new BluePotion(this, GetRandomLocation());
                    break;
                case 3:
                    Enemies = new List<Enemy>() { new Ghoul(this, GetRandomLocation()) };
                    WeaponInRoom = new Bow(this, GetRandomLocation());
                    break;
                case 4:
                    Enemies = new List<Enemy>()
                    {
                        new Bat(this, GetRandomLocation()),
                        new Ghost(this, GetRandomLocation())
                    };

                    if (!player.Weapons.Contains("Bow"))
                        WeaponInRoom = new Bow(this, GetRandomLocation());
                    else
                        WeaponInRoom = new BluePotion(this, GetRandomLocation());
                    break;
                case 5:
                    Enemies = new List<Enemy>()
                    {
                        new Bat(this, GetRandomLocation()),
                        new Ghoul(this, GetRandomLocation())
                    };
                    WeaponInRoom = new RedPotion(this, GetRandomLocation());
                    break;
                case 6:
                    Enemies = new List<Enemy>()
                    {
                        new Ghost(this, GetRandomLocation()),
                        new Ghoul(this, GetRandomLocation())
                    };
                    WeaponInRoom = new Mace(this, GetRandomLocation());
                    break;
                case 7:
                    Enemies = new List<Enemy>()
                    {
                        new Bat(this, GetRandomLocation()),
                        new Ghost(this, GetRandomLocation()),
                        new Ghoul(this, GetRandomLocation())
                    };

                    if (!player.Weapons.Contains("Mace"))
                        WeaponInRoom = new Mace(this, GetRandomLocation());
                    else
                        WeaponInRoom = new RedPotion(this, GetRandomLocation());
                    break;
                case 8:
                    MessageBox.Show("Congratulation! You have completed the game!");
                    Application.Exit();
                    break;
                default:
                    break;
            }
        }
    }
}
