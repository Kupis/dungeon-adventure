﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Dungeon_Adventure
{
    class Player : Mover
    {
        private Weapon equippedWeapon = null;

        public int HitPoints { get; private set; }

        private List<Weapon> inventory = new List<Weapon>();

        public IEnumerable<string> Weapons
        {
            get
            {
                List<string> names = new List<string>();
                foreach (Weapon weapon in inventory)
                    names.Add(weapon.Name);
                return names;
            }
        }

        public Player(Game game, Point location)
            : base(game, location)
        {
            HitPoints = 10;
        }

        public void Hit(int maxDamage)
        {
            int randomDamage = Game.random.Next(1, maxDamage);
            HitPoints -= randomDamage;
            Game.Logs.AppendLine("Player took " + randomDamage.ToString() + " damage from");
        }

        public void IncreaseHealth(int health)
        {
            int randomHealth = Game.random.Next(1, health);
            HitPoints += randomHealth;
            Game.Logs.AppendLine("Player health incresed by " + randomHealth.ToString());
        }

        public void Equip(string weaponName)
        {
            foreach (Weapon weapon in inventory)
                if (weapon.Name == weaponName)
                    equippedWeapon = weapon;
        }

        public void Move(Direction direction)
        {
            base.location = Move(direction, game.Boundaries);
            Game.Logs.Clear();
            Game.Logs.AppendLine("Player moved in " + direction.ToString().ToLower() + " direction");
            if (!game.WeaponInRoom.PickedUp)
                if (Nearby(game.WeaponInRoom.Location, 10))
                {
                    inventory.Add(game.WeaponInRoom);
                    game.WeaponInRoom.PickUpWeapon();

                    if (equippedWeapon == null)
                        equippedWeapon = inventory[0];
                }
        }

        public void Attack(Direction direction)
        {
            Game.Logs.Clear();

            if (equippedWeapon != null)
                equippedWeapon.Attack(direction);

            if (equippedWeapon is IPotion)
            {
                inventory.Remove(equippedWeapon);
                equippedWeapon = inventory[0];
            }
        }
    }
}
