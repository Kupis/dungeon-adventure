﻿using System;
using System.Drawing;

namespace Dungeon_Adventure
{
    abstract class Weapon : Mover
    {
        public bool PickedUp { get; private set; }

        public Weapon(Game game, Point location)
            : base(game, location)
        {
            PickedUp = false;
        }

        public void PickUpWeapon() { PickedUp = true; }

        public abstract string Name { get; }

        public abstract void Attack(Direction direction);

        protected bool DamageEnemy(Direction direction, int radius, int damage)
        {
            Point target = game.PlayerLocation;

            foreach (Enemy enemy in game.Enemies)
            {
                if (!enemy.Dead)
                {
                    if (Nearby(enemy.Location, target, radius))
                    {
                        enemy.Hit(damage);
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
