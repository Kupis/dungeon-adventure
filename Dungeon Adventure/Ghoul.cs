﻿using System;
using System.Drawing;

namespace Dungeon_Adventure
{
    class Ghoul : Enemy
    {
        public Ghoul(Game game, Point location)
            : base(game, location, 8, "Ghoul") { }

        public override void Move()
        {
            if (HitPoints <= 0)
                return;

            int randomResult = Game.random.Next(3);
            if (randomResult == 0 || randomResult == 1)
            {
                Direction playerDirection = FindPlayerDirection(game.PlayerLocation);
                location = Move(playerDirection, game.Boundaries);
                Game.Logs.AppendLine("Ghoul moved in player direction - " + playerDirection.ToString().ToLower());
            }
            else
                Game.Logs.AppendLine("Ghoul don't move");

            if (NearPlayer())
                game.HitPlayer(4);
        }
    }
}
