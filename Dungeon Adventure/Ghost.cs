﻿using System;
using System.Drawing;

namespace Dungeon_Adventure
{
    class Ghost : Enemy
    {
        public Ghost(Game game, Point location)
            : base(game, location, 8, "Ghost") { }

        public override void Move()
        {
            if (HitPoints <= 0)
                return;

            if (Game.random.Next(3) == 0)
            {
                Direction playerDirection = FindPlayerDirection(game.PlayerLocation);
                location = Move(playerDirection, game.Boundaries);
                Game.Logs.AppendLine("Ghost moved in player direction - " + playerDirection.ToString().ToLower());
            }
            else
                Game.Logs.AppendLine("Ghost don't move");

            if (NearPlayer())
                game.HitPlayer(3);
        }
    }
}
