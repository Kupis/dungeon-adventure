﻿namespace Dungeon_Adventure
{
    interface IPotion
    {
        bool Used { get; }
    }
}
