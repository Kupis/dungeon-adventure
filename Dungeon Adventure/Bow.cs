﻿using System;
using System.Drawing;

namespace Dungeon_Adventure
{
    class Bow : Weapon
    {
        public Bow(Game game, Point location)
            : base (game, location) { }

        public override string Name { get { return "Bow"; } }

        public override void Attack(Direction direction)
        {
            DamageEnemy(direction, 50, 1);
        }
    }
}
