﻿using System;
using System.Drawing;

namespace Dungeon_Adventure
{
    class Bat : Enemy
    {
        public Bat(Game game, Point location)
            : base(game, location, 6, "Bat") { }

        public override void Move()
        {
            if (HitPoints <= 0)
                return;

            if (Game.random.Next(2) == 0)
            {
                Direction randomDirection = (Direction)Game.random.Next(4);
                location = Move(randomDirection, game.Boundaries);
                Game.Logs.AppendLine("Bat moved in random direction - " + randomDirection.ToString().ToLower());
            }
            else
            {
                Direction playerDirection = FindPlayerDirection(game.PlayerLocation);
                location = Move(playerDirection, game.Boundaries);
                Game.Logs.AppendLine("Bat moved in player direction - " + playerDirection.ToString().ToLower());
            }

            if (NearPlayer())
                game.HitPlayer(2);
        }
    }
}
