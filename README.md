#Dungeon Adventure

.NET turn-based game about fighting enemies with weapons found in dungeon.
Application is based on book "Head First C#" O'REILLY Jennifer Greene, Andrew Stellman

## Purpose

The purpose of this application is to test current knowledge.

## Assumptions

### Control

To control player movement or attack use:
* Mouse to click buttons in right down corner,
* Buttons : WASD to move and IJKL to attack.

### Levels

There are 7 levels with enemies and weapons:
* 1 level: Bat; sword,
* 2 level: Ghost; blue potion,
* 3 level: Ghoul; bow,
* 4 level: Bat, ghost; Bow if player don't have, otherwise blue potion,
* 5 level: Bat, ghoul; red potion,
* 6 level: Ghost, ghoul; mace,
* 7 level: Bat, ghost, ghoul; mace if player don't have, otherwise red potion,
* 8 level: Game ends.

### Enemies
There are 3 enemies:
1. Bat - max damage 2, 6 health points, 50% to move in player direction, 50% to move in random direction,
2. Ghost -  max damage 3, 8 health points, 1/3 to move in player direction, 2/3 to stay in place,
3. Ghoul - max damage 4, 10 health points, 2/3 to move in player direction, 1/3 to stay in place.

### Weapons and potions
There are 3 weapons and 2 potions:
* Sword - 20 range, max 3 damage, attack in 3 directions: selected direction and 2 directions beside selected (for example if we select left direction, sword will atack in left, up and down direction),
* Bow - 50 range, 1 damage, attack only in selected direction,
* Mace - 30 range, max 6 damage, attack in all 4 directions,
* Blue potion - heal up to 5 helth points. Consumed after use,
* Red potion - heal up to 10 health points. Consumed after use.

## Author

* **Patryk Kupis** - [Linkedin](https://www.linkedin.com/in/patryk-kupis-12a453162/), [Gitlab](https://gitlab.com/Kupis)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.